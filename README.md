# IoT: Temperture sensor (wifi) & web front-end (php / own webserver)

![screenshot ; and with ardunio hardware](/screenshots/screenshot-graph_both.png)

Scroll down for [another screenshot](/screenshots/screenshot.png)

### contains:
- Script for Ardunio as Temperature Sensor for wifi (.ino)
- Script in PHP _(save & view the values on Server)_ (.php)


### features:
- web-frontend for Browser
- show/store data, simple chart/graph
- alarm / alert email on temperature _( in detail: temperature above or below ; send once, after the data recieved, and must be reseted ; value adjustable by user )_
- (optional:) alarm if sensor offline _(cronjo needed)_
- supports multiple temperture sensor
- (optional:) export(csv)
- (optional:) delete old data (manuell by hand)

### Fixed values in Code:
- WLAN key / station is once fixed (on your device)
- alarm email-adress is fixed on web-server (not changeable by user)

### limitations
- graph has a fixed range ~ -5° until 100°
  - workaround for higher values > 100 : search for ``$maxHeight``  and set change value
  - workaround for lower values < 0 : search for ``$border_bottom``  and set the a higher value (buggy: no background lines)
- recommend more for privat installation (for public available installation [add an api-key](https://gitlab.com/soerenj/wemos_temp-sensor-simple/raw/master/php/_optional__how_to_add_an_api-key.txt); Than only you can submit new values, and not anyone in the world)

### needed:
* Arduino-Board ( with wifi, like, alternativ Wemos, NodeMCU,... ; must have internet-access)
* temperatur sensor
> (i used temperatur sensors: "DS18B20" / "DS18S20": connect this sensor is very easy, you just need a 4,7K Ω resistor and three cables; and 3V _(?)_ or 5V from board (depending on your board-input accepting 5V or 3.3V); see [circuit](/screenshots/circuit.png) below )
* your own Webserver ( Webspace / Website ) with PHP and Mysql; ( *alternative* pay for external Webservice: [e.g. script](https://gist.github.com/soerenj/a25e7c0d455c443cfe710d7e8fbb35c8) )
* of course a Web-Browser ;-)


### this way it works:
* It wakes up every X-Seconds/Hours, measure the temperature, and send it to the server (via wifi).
  It should have low energy consume.
* The Wlan SSID/Password are hardcoded.

  
### How to install
 *  First: do some tests/experiments with DS18B20: [About Temperature Sensor DS18B20 on Ardunio](https://create.arduino.cc/projecthub/TheGadgetBoy/ds18b20-digital-temperature-sensor-and-arduino-9cc806)
 * Ardunio Sketch file
   * set wifi: ssid, passwort
   * set host (=web-server Hostname)
   * (optional:) set sleepTimeInSec to change your measure Period, default: each hour
   * put the script on your Ardunio
 * on your Webserver: MySql
    * create new user with passwort
    * create new database
 * on your Webserver: PHP
    * edit the ``temp-measure.php``-file: insert database username/passwort/database-Name
    * upload the ``temp-measure.php``-file ( must be in root/main-www directoy )
 * in your Browser
    * open the URL-adress (e.g. ``http://example-your-host/temp-measure.php``)
    * click create table
    * (optional:) print out a paper, with a [link/QR-Code](https://gitlab.com/soerenj/wemos_temp-sensor-simple/raw/master/sketch_wlan-temp-sensor.ino/just-a-paper-with-link-to-sourcecode-url/paper_to_print.pdf?inline=false) to this SourceCode (e.g. in case you must change the wifi password: you need this SourceCode again)
    
### inaccurate graph:
 * in short: the graph is a _bit_ inaccurate : not usable for science; not compare-ables between with old graphs-Images.
 * in detail: the problem are measure gap's (e.g. offline or non regular measure timing)
   * there be marked with color and values are estimated (see screenshot below).
   * the width of the graph (timeline) will scratched automatically,
     cause every graph have a fixed count of measure Points (e.g. 100)



not very well code, not beautiful programm code. But it's working.
Internet Of Things (IoT), DIY tempeature Sensor


## Other Versions (old)
* Version for just a single Sensor: [ Branch: single-sensor-old](https://gitlab.com/soerenj/wemos_temp-sensor-simple/tree/single-sensor-old)
* Version for just a single Sensor, whithout Graph: [ Branch: single-sensor-old-simple-no-graph](https://gitlab.com/soerenj/wemos_temp-sensor-simple/tree/single-sensor-old-simple-no-graph)

-----------

![screenshot full](/screenshots/screenshot.png)

![circuit](/screenshots/circuit.png)

---------------------------------------------------------------------------------
## the same in german / auf deutsch:
Internet der Dinge
Temperatur Sensor für Wlan, selbstbau, DIY, Do-It-Yourself.

### enthält:
- .ino: Script for Wemos ( or NodeMCU / Ardunio) as Temperature Sensor
- .php: PHP Script zum Speichern und ansehen der Werte

### funktionen1:
- zeige/speichere Daten, einfacher Graph/Grafik
- alarm email bei temperature (darüber oder darunter; einstellbar vom Benutzer; E-mail nur einmalig, dannach reset klicken )

### funktionen2:
- WLAN schlüssel / station ist fest eingestellt (fest je Gerät)
- merhrere  temperture Sensor gleichzeitig werden unterstützt
- alarm Emailadresse ist fest eingestellt (nicht änderbar für den Benutzer)


### Beschränkungen
- Graph zeigt nur zwischen ~ -5° und 100° an
  - workaround für höhere Werte > 100 : suche nach ``$maxHeight``  und setze den Wert
  - workaround für kleinere Werte < 0 : suche nach ``$border_bottom``  und setzte eine höheren Wert (buggy: keine Hintergrund Linien)
- eher für private installation empfohlen (für öffentlich erreichbare installationen sollte man [einen api-key hinzufügen](https://gist.github.com/soerenj/cffa826db55027479704cc39f2d4780d); Nur mit diesen Key, kann man dann neue Mess-Werte hinzufügen)

### man braucht:
* ein Wemos-Board (like/or NodeMCU,..., Ardunio with Internet-Access) mit Temperatur sensor
> (z.B. als Temperatur Sensor: "DS18B20" oder "DS18S20" : ist leicht anzuschließen, man braucht nur 1x 4,7K Widerstand und drei kabel; z.B. 3.3V bekommt man vom Board; Je noch Board/Kabellänge sollte man evtl. 5V als Spannung nehmen)
* einen Webserver / Webspace / Website mit PHP und Mysql; ( *alternativ* kann man für einen externen Dienst  bezahlen: [z.B. script](https://gist.github.com/soerenj/a25e7c0d455c443cfe710d7e8fbb35c8) )


###  wie funktioniert es:
* Es wacht alle X-Sekunden/Stunden auf, misst die temperatur und sendet sie an den Server (über Wlan)
  Sollte geringen stromverbraucht haben.
* Das Wlan-Netz SSID/Password sind fest in den Programm-Code eingestellt. Bei welchel in anderes Wlan muss man erst via Computer den Wemos neu bespielen.

Der Graph ist ungenau und nicht vergleichbar. Weitere Infos/Installation im englischen Text.

Kein besonders schöner Programmcode, aber er funktioniert.

## Andere Versionen (alt)
* Version für einen einzelnen Sensor: [ Branch: single-sensor-old](https://gitlab.com/soerenj/wemos_temp-sensor-simple/tree/single-sensor-old)
* Version für einen einzelnen Sensor, ohne Graph: [ Branch: single-sensor-old-simple-no-graph](https://gitlab.com/soerenj/wemos_temp-sensor-simple/tree/single-sensor-old-simple-no-graph)

