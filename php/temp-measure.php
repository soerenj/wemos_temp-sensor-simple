<?php
#IoT: Temperture sensor (wifi) & server back-end (php)
#https://gitlab.com/soerenj/wemos_temp-sensor-simple/
#Temperature Sensor (wifi) + Graph + Datastore (PHP/Mysql) + Script for wifi Ardunio (wemos) + Email-Alarm 
#save temperture values and view in web-frontend with graph; supports multiple sensors
#limitation: only 0-100° Crad Celius
# ( workaround for limitation: search for $maxHeight and set change value; search for $border_bottom and set the a higher value )
#Licence AGPL

//MAIN CONFIG//
//config Database
$dbname = "xxx"; 
$dbusername = "x";
$dbpassword = 'xxx';
$dburl='';
//MAIN CONFIG//


//title
$sensor_name = 'Temperatur Sensor - My Heating'; 
$sensor_name_add_title = 'Heating XYZ 200 At Home'; //additional for only for Page-Title

//(optional) email-alert
//to active the Email-Alert you must add a cronjob on your Server: The Cronjob should call this URL, e.g. each hour
$alarm_email = 'email@example.com'; //disabled if email@example.com inserted
$alarm_email_from = "From: Temperature Sensor <sensor@axample.com>"; //e.g.(take care of the text-structre; dont forget the "FROM: "): "From: My Temp Sensor <sensor@example.com>"

//(optional)
$addMetaTags = ''; //e.g. <meta name="robots" content="noindex">';

$allow_delete=false;//show option to delete old Data
$allow_export=false;//show option to export data as csv


//date_default_timezone_set('Europe/Berlin');

function customCodeFirstLine($first_row){
	/* just optional code... to show the current/last measure-Values
           just an example (must be changed by your own sensor-ids):
	if($first_row['21400e22']['timestamp']>time()-(60*60*2.5)){
		echo "<span style=\"margin-top:5pt;display:inline-block\"> ";
		if($first_row['21400e22']['timestamp']>time()-(60*60*1.5))
			echo "<small style=\"color:#bbbbbb\">as at ".date("H:i",$first_row['28ffe330']['timestamp'])."</small> <br>";
		if($first_row['21400e22']['timestamp']<=time()-(60*60*1.5))
			echo "as at: ".date("d.m.y H:i",$first_row['28ffe330']['timestamp'])."<br>";
		echo "Sensor1: <b>".$first_row['28ffe330']['temp']."°</b><br>";
		$d=$first_row['21400e23']['temp'] - $first_row['21400e24']['temp'];
		if($d>0)$c='color:#0033cc';elseif($d<0) $c='color:#000099';
		echo "Sensor2 - Sensor3: <b style=\"$c\">".( $d ).'°</b>';
		
		
		echo'</span><br>';
	}*/
}

$configDraw= array();
$configDraw['addationalBackgroundLine'] = array(); //addational Background-odtted line(s) (more ligher) at a value

// -- config end -- //


//Email-Alert state files
$alarm_temp = 60; //in Grad (just the default, value; can be changed in by User / User-Interface)
$alarm_file_temperature = 'temperatur_messer_alarm_email_temperature.txt'; 
if(file_exists($alarm_file_temperature) && file_get_contents($alarm_file_temperature)!='')
   $alarm_temp = (int)file_get_contents($alarm_file_temperature);
$alarm_file_option1 = 'temperatur_messer_alarm_email_option1.txt';  //above oder below temperature
if(file_exists($alarm_file_option1) && file_get_contents($alarm_file_option1)!='')
   $alarm_option1 = substr(file_get_contents($alarm_file_option1),0,5); else $alarm_option1 = 'below';
$alarm_file_t_id = 'temperatur_messer_alarm_t_id.txt';  //which sensor (on multiple-once)
if(file_exists($alarm_file_t_id) && file_get_contents($alarm_file_t_id)!='')
    $alarm_t_id = substr(file_get_contents($alarm_file_t_id),0,8); else $alarm_t_id = '';


//database (e.g. mysql):
$stmt_create_table1 = "
CREATE TABLE `temp_sensor_data` (
  `id` int(11) NOT NULL,
  `temp` double NOT NULL,
  `press` double NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `t_id` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$stmt_create_table2 = "
ALTER TABLE `temp_sensor_data`
  ADD PRIMARY KEY (`id`);";

$stmt_create_table3 = "
ALTER TABLE `temp_sensor_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
";






if(isset($_GET["export"]) && $allow_export===true){
	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {ob_start('ob_gzhandler');} //does this work?
 	$stmt = "SELECT * FROM `temp_sensor_data` ORDER BY timestamp";
    $connection = getDBConnection();
    $result = $connection->query($stmt); if($result===false)return array();
	header("Content-type: text/csv");
	header('Content-Disposition: attachment; filename="temperatur_messer_export_'.date('y-m-d_H-i').'.csv"');
	$isFirstRow=true;
	while($row = ($result->fetch_assoc())){
		if($isFirstRow){
		  foreach($row as $key=>$item){
		     echo $key;
			 if(array_key_last($row)!=$key) echo",";
		  }
		  echo ",datetime";
		  echo "\n";
		}
		foreach($row as $key=>$item){
		   echo $item;
		   if(array_key_last($row)!=$key) echo",";
		}
		echo ",".date("d.m.Y H:i:s",$row["timestamp"])."";
		echo "\n";
		$isFirstRow=false;
	}
    die("");
}




if( isset($_GET['stat_image']) ){ readDataAndDrawStat( isset($_GET['limit'])?$_GET['limit']:null , $configDraw );exit; }

error_reporting(E_ALL);
ini_set('display_errors', 1);
echo '<!DOCTYPE html><html>
<head>
<title>🌡Temperatur sensor '.$sensor_name.' '.$sensor_name_add_title .'</title>
'.$addMetaTags.'
<style>
@media (pointer:coarse) or (max-width: 780px) {
 #maingraph_scroll a, #maingraph2_scroll a, a{
	padding: 5pt;
	background: #ddd;
	background: linear-gradient(#dfdfdf, #dadada, #fff);
	border-radius: 5pt;
	margin: 5pt;
	line-height: 15pt;
	display: inline-block;
	border-style: inset;
	
 }
 a#showEmailAlertSettings{
  border-top-right-radius: 0pt;
  border-bottom-right-radius: 0pt;
  border-right: 0pt;
 }
}

form#email_alarm_form{
 border: inset black 1pt;
 border-radius: 5pt;
 padding: 2pt;
}
acronym{ cursor: help; }
</style>
</head><body>';


//validate t_id
if(isset($_GET['t_id']) && $_GET['t_id']!='' && preg_match("#^[0-9abcdef,]*$#", $_GET['t_id'])!==1)
   die("please check t_id. Only hex values are allowed");

//email-Alarm set Temperature
if(isset($_POST['set_alarm_temperture']) ){
	#set_alarm_temperture
    if(! ($fp=fopen($alarm_file_temperature,'w')) ) echo'<span style="color:red">error</span> Failed to set Alarm-Temperature (failed writing file '.$alarm_file_temperature.')';
	if(!fwrite($fp,(int)$_POST['set_alarm_temperture'])){echo "<span style=\"color:red\"> Failed: Alarm-Temperature is set.  ".($_POST['set_alarm_temperture'])."°</span> <br>";$alarm_temp=(int)$_POST['set_alarm_temperture']; }
	
	#set_alarm_t_id
	if($_POST['set_alarm_t_id']=='')@unlink($alarm_file_t_id);
	else{
	  if(! ($fp=fopen($alarm_file_t_id,'w')) ) echo'<span style="color:red">error</span> Failed to set Alarm-Selection Sensor(failed writing file '.$alarm_file_t_id.')';
	  if(!fwrite($fp,substr($_POST['set_alarm_t_id'],0,8))){echo "<span style=\"color:red\"> Failed: Alarm-Temperature Sensor-Selection.  ".($_POST['set_alarm_t_id'])."</span> <br>";$alarm_t_id=substr($_POST['set_alarm_t_id'],0,8); }
	}
	
	#set_alarm_option1
	if(! ($fp=fopen($alarm_file_option1,'w')) ) echo'<span style="color:red">error</span> Failed to set Alarm-Temperature-Option1 (failed writing file '.$alarm_file_option1.')';
	if(fwrite($fp,substr($_POST['set_alarm_option1'],0,5))){echo "<span style=\"color:green\"> Alarm-Temperature is set.  ".substr($_POST['set_alarm_option1'],0,5)."".($_POST['set_alarm_temperture'])."° &nbsp; &nbsp; $alarm_t_id</span> <br>";$alarm_option1=substr($_POST['set_alarm_option1'],0,5);}
	else echo'<span style="color:red">error</span> Failed to set Alarm-Temperature (failed writing file '.$alarm_file_option1.')';
	
}
if(!is_writable('./')) echo ("<p>Error: Folder not writeable (this is only for the automatic Alarm-Emails, nothing else)</p>");





//email-Alarm (reset)
$alarm_file_state = 'temperatur_messer_alarm_email_state.txt';
if(isset($_GET['reset']) && $_GET['reset']=="1"){
	if(unlink($alarm_file_state))echo "<span style=\"color:green\"> Alarm is reseted.</span> <br>";
	else echo'<span style="color:red">error</span> Failed to reset Alarm (delete file '.$alarm_file_state.')';
}
if(!is_writable('./')) echo ("<p>Error: Folder not writeable (this is only for the automatic Alarm-Emails, nothing else)</p>");
if(file_exists($alarm_file_state)) echo( "<p align=\"right\">Alarm-Email currently disabled: <small style=\"color:#999999\">Last Alarm-Email (=above/below Temperature) was already send. It musst be reset, for receive new Alarm-Emails: <a href='?reset=1'>Email-Alarm reset</a></small> )</p>" );



//receive values
if (isset($_GET) && (isset($_GET['temp']) || isset($_GET['press']))) {

	echo "<h2>Parameter recieved:</h2>";
	$tempValue = 0;
	$pressValue = 0;
	$milliseconds = round(microtime(true));
	foreach ($_GET as $key => $value) { 
		echo "parameter: ";
		echo strip_tags(substr($key,0,6));
		echo "<br/>Value:";
		echo strip_tags(substr($value,0,6));
		echo "<br/><br/>";
		if($key === 'temp'){
			$tempValue = $value;
		} else if($key === 'press'){
			$pressValue = $value;
		}else if($key === 't_id'){
			$t_id = $value;
		}
	}
	echo "Timestamp: ";
	echo date('d.m.Y H:i:s',$milliseconds);
	echo " (<i>automatic</i>)";
	echo "<br/>";    
	storeData($tempValue, $pressValue, $milliseconds, $t_id);
	if( ($alarm_option1=='above' && $tempValue>=$alarm_temp) || ($alarm_option1=='below' && $tempValue<=$alarm_temp) ){
		if(!file_exists($alarm_file_state) &&
		    ($t_id=='' || $alarm_t_id==$t_id || $alarm_t_id==substr($t_id,0,strlen($alarm_t_id))) && 
		     strpos($alarm_email,"@example.com")===false 
		  ){
			if( sendAlarm($tempValue,$alarm_temp, $sensor_name,$alarm_email,$alarm_email_from) ){
				$myfile = fopen($alarm_file_state, "w") or die("Unable to open file! $alarm_file_state");
				$txt = "Alarm mail was send\n";
				fwrite($myfile, $txt);
				fclose($myfile);
			}else echo "failed to send Alarm mail";
		}
	}
	exit;
}



//email-Alarm (setup for users)
if($alarm_option1=='above')$c_above='selected="selected"'; else $c_above='';
if($alarm_option1=='below')$c_below='selected="selected"'; else $c_below='';
$sensors = getSensors();$s_inactiv='';$t_inactiv='';
if(strpos($alarm_email,"@example.com")!==false){ $s_inactiv=";opacity:0.1;";$t_inactiv=" title=\"first set up an Email-Adress in config\" ";}

//$hasMoreThanSingleSensor:
if(count($sensors)>0 || (count($sensors)==1 && isset($sensors[$alarm_t_id])) || (count($sensors)<1 && $alarm_t_id!='') )$hasMoreThanSingleSensor=true; else $hasMoreThanSingleSensor=false;

//alarm inactive
$inActiveEmailAlarmNote ='';
if(!in_array($alarm_t_id,$sensors) && strpos($alarm_email,"@example.com")===false){
	if(  (count($sensors)>=1 && !isset($sensors[$alarm_t_id]))  ||  ((count($sensors)<1 && $alarm_t_id!=''))  )
		$inActiveEmailAlarmNote = "<p style=\"color:blue\">&#x26a0; Email-Alert possible inactive: you must select a new Sensor</p>";
}
echo "<p id='email_alarm_span_fixed' style='text-align:right; float: right;$s_inactiv' $t_inactiv >
<a href='#' id='showEmailAlertSettings' onclick=\"document.getElementById('email_alarm_form').style.display='block';this.parentElement.style.display='none';\" title='change Email-Alert'>Email-Alarm</a> ";
if($inActiveEmailAlarmNote!='')echo $inActiveEmailAlarmNote;
else echo "at <acronym onclick='alert(this.title)' title='sensor ID' >$alarm_t_id</acronym> $alarm_option1 $alarm_temp ° ";
echo "</p>";

echo "<form method='post' action='". $_SERVER["SCRIPT_URI"]."' id='email_alarm_form' style='display:block;float:right'>Email-Alarm at Sensor ";

if($hasMoreThanSingleSensor ){
	echo  "
	<select title='select a sensor id' name='set_alarm_t_id' >";
	foreach($sensors as $s=>$empty){
	  if($s==$alarm_t_id) $state='selected'; else $state='';
	  echo"\n<option $state >$s</option>";
	}
	if(count($sensors)<1 && $alarm_t_id!='')echo"\n<option ></option>";
	echo "\n</select>";
}

echo "&nbsp;<select name='set_alarm_option1' >\n	<option name='above' value='above' $c_above title='&gt; above ; values higher then'>&gt; (above)</option> \n	<option name='below' value='below' $c_below title='&lt; below ; lower than'>&lt; (below)</option> \n</select> ";

echo $inActiveEmailAlarmNote;
echo "<input type='text' size='3' name='set_alarm_temperture' value='$alarm_temp' length='3' />° \n <input type='submit' id='set_alarm_temperture_submit' name='' value='change email alert' />\n 
</form>
<script language='javascript'> document.getElementById('email_alarm_form').style.display='none';</script>
";


if(isset($_POST['delete_older_then']) && $allow_delete===true){
  $_POST['delete_older_then']=(int)$_POST['delete_older_then'];
  if($_POST['delete_older_then']==0) die("nothing to delete, please go back");
  if($_POST['delete_older_then']<7) die("nothing to delete, lower then last 7Days not allowed to delete. please go back");
  echo "<hr>";
  echo "<h1 style=\"color:red\">DELETE Data older then ".(int)$_POST['delete_older_then']." days<h1>";
	getDBConnection();
    $stmt = "DELETE FROM `temp_sensor_data` WHERE `timestamp` < ".(time()-($_POST['delete_older_then']*60*60*24))." ORDER BY timestamp DESC";
    $connection = getDBConnection();
    $result = $connection->query($stmt);
    //echo $stmt;
	if($result===true)echo "<p>erfolgreich durchgeführt</p>"; else "<p>Fehler?</p>";
	var_dump($result);
  echo "<hr>";
}




//show values/graph
echo "<h1 style=\"display:inline;font-weight:normal\">🌡 Temperatur Sensor ".$sensor_name."</h1><br>";
echo readData();
echo "<hr>";
echo "<font size=2 color=\"#555555\"><br>api/how to save data: ";  
echo "possible parameters: temp - Temperatur, press - air pressure;&nbsp;&nbsp;&nbsp;&nbsp;";
$sampleLink = "".$_SERVER['PHP_SELF']."?temp=12.4&press=54.5&t_id=03ff0135 (sensorID hex 8length)";
if($allow_delete===true)echo "<details><summary>Delete Data (click for details): </summary>
<span class=\"opacity_nonhover\">Clean Up Database: delete all data older then 
<form method=\"post\" id=\"form_delete\" style=\"display:inline\"><select name=\"delete_older_then\" id=\"delete_older_then\">
<option value=\"\">-</option><option value=\"1085\">3year</option><option value=\"720\">2year</option><option value=\"365\">1year</option><option value=\"182\">1/2year</option><option value=\"90\">90days</option><option value=\"30\">30days</option><option value=\"7\">7days</option><option value=\"\">-</option>
</select>
<input type=\"button\" value=\"delete\" onClick=\"if(document.getElementById('delete_older_then').value>0 && prompt('DELETE the Data? \\n(can NOT be undone)\\nplease write \\'delete really\\'')=='delete really')document.getElementById('form_delete').submit()\"></form></span>
</details>
<hr>";
if($allow_export===true)echo "<font size=2 color=\"#555555\"><a href=\"?export=csv\">Export Data(csv)</a></font></br>";
echo "zbsp.: $sampleLink</font>";
echo "<br><font size='1'><a href='https://gitlab.com/soerenj/wemos_temp-sensor-simple' rel='noreferrer'>source code</a> <i> (also from Temperatur Wlan-Sensor itself, Mikroprozessor Wemos)</i></font>";






//now followes only functions...



function storeData($tempValue, $pressValue, $milliseconds, $t_id){
    if(preg_match("#[^0-9,\.-]#",$tempValue) || preg_match("#[^0-9,\.-]#",$pressValue)){
      die("<font color=red><b>Fehler:</b></font>Not allowed Values (only: 0-9 , . )");
    }
	
    $connection = getDBConnection();
	
    $stmt = "INSERT INTO `temp_sensor_data` ";
    $stmt .= "(`temp`, `press`, `timestamp`, `t_id`)";
    $stmt .= " VALUES (";
    $stmt .= "'";
    $stmt .= mysqli_real_escape_string($connection,substr($tempValue,0,6));
    $stmt .= "',";
    $stmt .= "'";
    $stmt .= mysqli_real_escape_string($connection,substr($pressValue,0,6));
    $stmt .= "',";
    $stmt .= "'";
    $stmt .= $milliseconds;
    $stmt .= "',";
    $stmt .= "'";
    $stmt .= mysqli_real_escape_string($connection,substr($t_id,0,8));
    $stmt .= "'";
    $stmt .= ");";
    $result = mysqli_query($connection,$stmt);  
    if($connection->error)die("Failed to write in Datebase ".var_dump($connection->error));
}

function getSensors(){
    getDBConnection();
    $stmt = "SELECT `t_id` FROM `temp_sensor_data` GROUP BY `t_id` ORDER BY t_id DESC LIMIT 100";
    $connection = getDBConnection();
    $result = $connection->query($stmt); if($result===false)return array();
	$wert = array();
	while($row = ($result->fetch_assoc())){
		if($row['t_id']!='')$wert[ $row['t_id'] ] = '';
	}
	return $wert;
}
	
function readData(){
    getDBConnection();
    $stmt = "SELECT * FROM `temp_sensor_data` ORDER BY timestamp DESC LIMIT 100";
    $connection = getDBConnection();
    $result = $connection->query($stmt);
    if($result===false && strpos(mysqli_error($connection),"doesn't exist")!==false)die("Possible you must first create the Datatable? <a href=\"".$_SERVER['PHP_SELF']."?createDatabaseTable=1\">click here</a><br>".mysqli_error($connection));
	
	$wert = array();$wert_all_for_break=array();$first_row=array();$t_start='';$t_end='';
	while($row = ($result->fetch_assoc())){
		if(!isset($wert[$row['t_id']]))$wert[$row['t_id']]=array();
		$wert[$row['t_id']][ $row['timestamp'] ] = $row;
		if( $row['t_id']!='' && !isset($first_row[ $row['t_id'] ]) ){
			//take von every the first=newest line; but only if there are nealy the same timestamp
			$first_row[$row['t_id'] ] = $row;
		    if($row['timestamp']>(current($first_row)['timestamp']+360)) $first_row =array();
		}
		if($t_start=='')$t_start=$row['timestamp'];
		$t_end=$row['timestamp'];
	}
	if(!function_exists('imagecreate')) echo("<p>No Graph: missing php gd-image libary e.g. install package php-gd (or simular) </p>");

	//if(isset($_GET['a']))var_dump(  );//[0]['timestamp'];
	$default_measureGap_inMin = getDefault_measureGap_inMin($wert);
	if(function_exists('customCodeFirstLine')) echo customCodeFirstLine($first_row);
	if(($t_start-$t_end)>60*60*6)$jump_offset = round ( ($t_start - $t_end)/60/60/6 )*60*60;
	else $jump_offset = round ( ($t_start - $t_end)/6 );
	//if(count($wert)>1)echo "<br><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1&limit=50\"  /><br><hr>";
	//if(count($wert)>1)echo "<br><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1&limit=300\"  /><br><hr>";
	$o='';//$o='';if(isset($_GET['offset'])) $o = '&offset='.(int) $_GET['offset'];
	$o_backw='&offset='.$t_start;//if(isset($_GET['offset'])) $o_backw = '&offset='.(((int) $t)-(60*60*12));
	$o_start=$t_start;
	    if(count($wert)>1)echo "<p id=\"maingraph_scroll\" style=\"margin-bottom: 0pt;margin-left: 1063px;\">
	scroll
	<a href=\"#\" data-id=\"maingraph\" data-direction=\"backwards\" title=\"backward main Graph (next one)\"><</a>
	<a href=\"#\" data-id=\"maingraph\" data-direction=\"forward\" title=\"forward main Graph (next one)\">></a></p><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1$o_backw\" id=\"maingraph\" data-offsetstart=\"$o_start\"  /><br>";
	
		if(count($wert)>1)echo "<hr><p id=\"maingraph2_scroll\" style=\"margin-bottom: 0pt;margin-left: 1063px;\">
	scroll
	<a href=\"#\" data-id=\"maingraph2\" data-direction=\"backwards\" title=\"backward main Graph (next one)\"><</a>
	<a href=\"#\" data-id=\"maingraph2\" data-direction=\"forward\" title=\"forward main Graph (next one)\">></a></p><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1$o_backw&limit=".(count($wert)*100)."\" id=\"maingraph2\" data-offsetstart=\"$o_start\"  /><br>";
	
		if(count($wert)>1)
		echo "
		<script language=\"javascript\">var offset=$t_start;
			document.getElementById('maingraph_scroll').getElementsByTagName('a')[0].addEventListener('click',scroll_foward);
			document.getElementById('maingraph_scroll').getElementsByTagName('a')[1].addEventListener('click',scroll_foward);
			
			document.getElementById('maingraph2_scroll').getElementsByTagName('a')[0].addEventListener('click',scroll_foward);
			document.getElementById('maingraph2_scroll').getElementsByTagName('a')[1].addEventListener('click',scroll_foward);

			function scroll_foward(event){
			    var direction = this.getAttribute('data-direction');
				var id = this.getAttribute('data-id');
				var img = document.getElementById(id);
				var offsetStart = img.getAttribute('data-offsetstart');
				var src = img.getAttribute('src');
				var a = src.match('offset=([0-9]*)');
				if(a==0)return;
				var offset = parseInt(a[1]);
				if(direction=='forward')var offset_new = (offset+$jump_offset);
				if(direction=='backwards')var offset_new = (offset-$jump_offset);
				if(offset_new>parseInt(offsetStart) && offsetStart!=null ){ event.preventDefault(); return; }
				img.src = src.replace('offset='+offset+'','offset='+offset_new+'');
				event.preventDefault();
			}
		</script>";
	
	
	echo "<br><br><hr><br><br><h2>Each single Sensor</h2><br><br>";
	if(count($wert)==0)echo "No Data: Currently the database-table is empty";
	foreach($wert as $t_id=>$w){
	  krsort($w);
	  if(count($wert)>1)echo '<h3>sensor:'.$t_id.'</h3>';
	  printTable($w,$t_id,$default_measureGap_inMin);echo "<br><br>";
	}
	
	echo "<hr>";
	echo "<br><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1&limit=1000\" title=\"1.000 entrys\" /><br><hr>";
    echo "<br><img src=\"".$_SERVER['PHP_SELF']."?stat_image=1&limit=10000\" title=\"10.000 entrys\" />";
}

//only once for installation needed
function createDBTable($link){
	global $create_query1, $create_query2, $create_query3;

	if($link->query($create_query1))echo "DB-Table created<br>"; else die("error on creating DB-Table".mysqli_error($link));
	if($link->query($create_query2))echo "DB-Table modifed<br>"; else die("error on creating DB-Table".mysqli_error($link));
	if($link->query($create_query3))echo "DB-Table modifed<br>"; else die("error on creating DB-Table".mysqli_error($link));
	if (mysqli_connect_errno()) {
		die( mysqli_error($link) );
	}
}


function getDefault_measureGap_inMin($wert_multiple){
	//found default time between measures
	$wert=array();
	foreach($wert_multiple as $w){
		foreach($w as $t=>$w2){
		  $wert[$t] = $w2;
		}
	}
	$wert_all_for_break=array();
	foreach($wert as $t=>$w){
		$new_t = round($t/10)*10; //to ignore the very short breaks between sending each single sensor data
		$wert_all_for_break[ $new_t ] = $w;
	}
    $breaks = array();
	$lastTimestamp=0;
	foreach( $wert_all_for_break as $timestamp=>$w){
	    if($lastTimestamp>0)@$breaks[ round(($lastTimestamp-$timestamp)/60/10)*10 ]++; 
	    $lastTimestamp = $timestamp;
	}
	arsort($breaks);
	return key($breaks);
}

function printTable($wert,$t_id,$default_measureGap_inMin){
	//print data-Table
	echo "<img src=\"".$_SERVER['PHP_SELF']."?stat_image=1&t_id=$t_id\" title=\"100 entrys\" />";
	echo "<table>";
	$i=0;$lastRow=null;
	foreach($wert as $timestamp=>$row){
	  if($lastRow!=null){ //temp diff
	    $diff = ($lastRow['temp']-$row['temp']);
	    $arrow="<span style=\"color:green\">↑</span>";
	    if(round($diff,3)==0) $arrow="";
		if($diff<0)$arrow="<span style=\"color:red\">↓</span>";
	    $diff=number_format(round($diff,3),2,'.',' ');
	    if($diff>0)$diff="+ ".$diff;
		if($diff<0)$diff="- ".str_replace('-','',$diff);
	    $t= ($lastRow['timestamp']-$row['timestamp'])/60/60; //each Hour
	    echo "</td>";
	    echo "<td style=\"color:#777777\">$arrow</td>";
	    echo "<td style=\"color:#777777;text-align:right\">".$diff." </td>";
		$diff_s_r=round(($lastRow['timestamp']-$row['timestamp']-(60*10))/100)*100/60;
		if($diff_s_r>$default_measureGap_inMin)$s='#555555;';
		else  $s='#cccccc';
	    echo "<td style=\"color:$s\">/ ".round($t,1)."h.</small>";
	  }
	  echo "</td></tr>";
      echo "<tr><td>".date('d.m.y H:i',$row['timestamp'])."</td><td>";
	  if($i==0 && $row['timestamp']>time()-60*60*3)echo "<b style=\"font-size:15pt\">";
	  echo "".$row['temp']." C°";
	  if($i==0 && $row['timestamp']>time()-60*60*3)echo "</b>";

	  $i++;
	  $lastRow = $row;
	}
	echo "</td></tr>";
	echo "</table>";
}

function sendAlarm($tempValue, $alarm_temp, $sensor_name, $email, $email_from=''){
	$text = "Here is writing your temperature-Sensor ".$sensor_name.". Alarm. The temperature is lower than ".$alarm_temp."°.\n current temperatur: ".$tempValue."\nThis email will be send only once. You must reset the alarm for further emails. To Reset open this site: ".$_SERVER['HTTP_HOST']."".$_SERVER['PHP_SELF']."?reset=1\nListe of Temperatur: ".$_SERVER['HTTP_HOST']."".$_SERVER['PHP_SELF'];
	if( mail($email,$sensor_name,$text, $email_from) ){ echo "<p>ALARM-email was send</p>"; return true;}else{ echo "failed to send Alarm-Email"; return false;}
}

function getDBConnection(){
	global $dbname, $dbusername, $dbpassword, $dburl;
    $link = mysqli_connect($dburl, $dbusername, $dbpassword,$dbname);
    if (!$link) {
      die('Failed to connect to Database: ' . mysqli_error($link));
    }
    if (mysqli_connect_errno($link)) {
      die( mysqli_error($link) );
    }
	
    if(isset($_GET['createDatabaseTable']))createDBTable($link);
    return $link;
}


function readDataAndDrawStat($limit=100,$configDraw=array()){
	
	$connection = getDBConnection();
	if( isset($_GET['t_id']) && $_GET['t_id']!='' && isset($_GET['t_id']) && preg_match("#^[0-9abcdef,]*$#", $_GET['t_id'])!==1) die('please check t_id value');
	if($limit==''){
	  $limit = 100;
		if(isset($_GET['t_id']))$limit = 100;
		if(count(getSensors())%3==0)$limit = 99;
	}
	
	//if($_GET['t_id']!='' && isset($_GET['t_id']) && preg_match("#^[0-9abcdef]*$#", substr($_GET['t_id'],8))===1) $where = ' WHERE t_id = \''.mysqli_real_escape_string($connection, $_GET['t_id']).'\'';else $where='';

	$where = '';
	if(isset($_GET['t_id']))
    {
      foreach(explode(',',$_GET['t_id']) as $val ){
		if(preg_match("#^[0-9abcdef]*$#", substr($val,8))!==1 || $val=='')continue;
		if($where=='')$where = ' WHERE '; else $where.=' or ';
		$where .= ' t_id = \''.mysqli_real_escape_string($connection, $val).'\'';
	  }
    }
	if(isset($_GET['offset'])){
		if($where=='')$where=' WHERE '; else $where.=' AND ';
		$where .= ' `timestamp` <= '.(int) $_GET['offset'];
	}
	$stmt = "SELECT * FROM `temp_sensor_data` $where ORDER BY timestamp DESC LIMIT  ".(int)$limit ;
    
    $result = $connection->query($stmt); 
	  if($result===false)die('no values');

	  $werte = array();$werte_single_lines=array();
	  while($row = ($result->fetch_assoc())){
		  $werte_single_lines[ $row['t_id'] ][ $row['timestamp'] ] = $row['temp'];
		  $werte[ $row['timestamp'] ] = $row['temp']; //possible deletes Values at same time
	  }
	  draw('test',$werte,$werte_single_lines,$configDraw);
}


 
function draw($title, $wert, $werte_single_lines,$configDraw=array()){
	
      $addationalBackgroundLine = array();
      if(isset($configDraw['addationalBackgroundLine']))$addationalBackgroundLine = $configDraw['addationalBackgroundLine'];
	
	  $width = '1000';
	  $height = '250';
	  $border_bottom = 70;
	  $border_right = 150;
	  $imgWidth = $width+$border_right;
	  $imgHeight = $height+$border_bottom;

      if(!function_exists('imagecreate')) die("missing php gd-image libary e.g. install package php-gd (or simular) ");
	  $theImage  = imagecreate($imgWidth, $imgHeight);
	  imagecolorallocate ( $theImage , 255, 255, 255);
	  $colorGrey = imagecolorallocate($theImage, 215, 215, 215);
      $colorGreyBlack = imagecolorallocate($theImage, 105, 105, 105);
	  $colorBlue = imagecolorallocate($theImage, 0, 50, 255);
	  $colorOrange1 = imagecolorallocate($theImage, 255, 157, 5);
	  $colorOrange2 = imagecolorallocate($theImage, 255, 119, 5);
	  $colorRed = imagecolorallocate($theImage, 255, 5, 5);
	  $black = imagecolorallocate($theImage, 0, 0, 0);
	  $gray  = imagecolorallocate($theImage, 102, 102, 102);
	  $grayLight  = imagecolorallocate($theImage, 182, 182, 182);
	  $white = imagecolorallocate($theImage, 255, 255, 255);
	  
	  $colorDiagramm_1 = imagecolorallocate($theImage, 158, 95, 12);#orange
	  $colorDiagramm_2 = imagecolorallocate($theImage, 38, 191, 0); #green
	  $colorDiagramm_3 = imagecolorallocate($theImage, 255, 108, 226);#violet
	  $colorDiagramm_4 = imagecolorallocate($theImage, 108, 192, 255);#blue light
      $colorDiagramm_5 = imagecolorallocate($theImage, 255, 243, 108);#like yellow
      
	  $maxHeight = 100;
	  //$maxValue = 100; //not used
	  //$minValue = 0;
	  //if(!isset($_GET['maxValue']) && !isset($_GET['minValue']))$maxHeight = 100;
	  //if(isset($_GET['maxValue']))$maxValue = (int)$_GET['maxValue'];else $maxValue = 100;
	  ////$maxHeight = $maxValue;
	  //if(isset($_GET['minValue']))$minValue=(int)$_GET['minValue'];else $minValue = 0;
      //for furute: if(isset($_GET['autoScale'])){ $maxHeight = 0; $maxValue=0;$minValue=0; }
	  #based on from https://webmaster-glossar.de/zeichnen-mit-php-ein-liniendiagramm.html
	  foreach($werte_single_lines as $w) $countBalken += count($w); //do not use: count($wert); due to same measureson the same time, count coul be wrong
	  /* not used (usefull for autoscale)
	  foreach($wert as $w){
		  if($maxHeight < $w){
			  //$maxHeight = $w;
			  $maxValue = $w;
		  }
		 // if($minValue >= $w){
		//	  $minValue = $w;
		//  }
	  }
	  
	  if($maxHeight==1){
		  foreach($wert as &$w){
			  $w = $w*25;
			  if($maxHeight < $w){
				  //$maxHeight = $w;
			  }
		  }
	  }*/
	  
	  


	
	  $default_measureGap_inMin = getDefault_measureGap_inMin($werte_single_lines);
	  $rangeValue = $maxHeight;
	  //$rangeValue = $maxHeight-$minValue;
	  //if($minValue<0) $rangeValue = $maxHeight+($minValue*-1);
	  $background_lines_at = array($rangeValue,$rangeValue/4*3,$rangeValue/4*3,$rangeValue/2,$rangeValue/4,0);
	  $background_lines_at = array_merge($background_lines_at,$addationalBackgroundLine); // your own special Background-Line
      
	
	  $dynWidth = ($imgWidth-2) / ($countBalken-1);
	  @$dynHeight = ($height/$rangeValue) * ( ($rangeValue) / $rangeValue ); ///($rangeValue-1)/$maxHeight
      $x_ori = 0;
      $y_ori = 0;
      $y_bottom = 0;
      $xlen = $width - $x_ori - $x_ori * 1.5;
      $ylen = $height - $y_ori - $y_bottom;
	  //die($dynHeight.'a');
	  #draw background lines
      foreach($background_lines_at as $lineValue){
          //old:$y = ($arry[$i] - $arry[0]) * $ylen/($arry[3]-$arry[0]);
          $y = $height - @round($dynHeight * $lineValue);
          $c = $black;
          $style=array($white,$white,$gray,$white,$white);
          if(in_array($lineValue,$addationalBackgroundLine)){
                    $style=array($white,$white,$grayLight,$white,$white);
                    $c = $grayLight;
          }
          imagesetstyle($theImage,$style);
          imageline($theImage,$x_ori+2,$y,$x_ori+70+40 + $xlen -2,$y,IMG_COLOR_STYLED);
          if($y<(4)) $y=$y+4; // otherwise the number on top, are cutted
          if($lineValue<10)$xd=10; else if($lineValue<100)$xd=5; else $xd=0; //right align of text-numbers
          if(($lineValue*10)%10==0) $value = number_format($lineValue,0);
          else $value = number_format($lineValue,1);
          imagestring($theImage,2,$x_ori+70+40 + $xlen + 5 + $xd,$y-7,$value, $c);               
      }
    ksort($wert);
    reset($wert);
    $timestampStart  = key($wert);
    end($wert);
    $timestampEnd    = key($wert);
    $timestampLength = $timestampEnd - $timestampStart;
    reset($wert);
    
    $eachSeconds_inPixel = $xlen/$timestampLength;
    if(isset($_GET['debug']))echo "xlen $xlen <br>";
    if(isset($_GET['debug']))echo "ylen $ylen <br>";
    //die($timestampLength);
    
    
    #draw dates
    $timestamp = $timestampStart;
    $last_t='';
    $last_day='';
    $last_m='';
    while($timestamp<=$timestampEnd){
      $tObje = new DateTime(  );
      $tObje->setTimestamp($timestamp);//die( $tObje->format('H'));
      $tObje->setTime( $tObje->format('H'), 0, 0);
      $timestamp=$tObje->getTimestamp(); //echo($timestamp.$tObje->getTimestamp().'a');
      $x = $eachSeconds_inPixel * ($timestamp-$timestampStart);
		
      if($x>0 && $last_day!=date('d.M.Y',$timestamp) && $x>20){
        if($countBalken<301){
            imagestring($theImage,2,$x,$imgHeight-45,"|".date('d.M.',$timestamp), $black);
        }elseif( $countBalken<601){
            if($last_m!= date('M',$timestamp)){
				imagestring($theImage,2,$ix,$imgHeight-25,"|".date('M',$timestamp), $black);
				$last_m = date('M',$timestamp);
			}
			imagestring($theImage,1,$x,$imgHeight-45,"|".date('d',$timestamp), $black);
        }else{
            if($last_m!= date('M',$timestamp)){ 
              imagestring($theImage,2,$x,$imgHeight*25,"|".date('M',$timestamp), $black);
			 $last_m = date('M',$timestamp);
            }
            if($countBalken<1001 ){
				if(date('d',$timestamp)%2==0) imagestring($theImage,1,$x,$imgHeight-45,"|".date('d',$timestamp), $black);
			}else{
				if(date('d',$timestamp)%10==0) imagestring($theImage,1,$x,$imgHeight-45,"|".date('d',$timestamp), $black);
			}
		}
      }
	  
      //draw "hours"
		  $lineHour = 6;
		  if( $countBalken>101 ) $lineHour = 12;
		  if( $countBalken>301 ) $lineHour = 25;
		  if(count($werte_single_lines)>1) $lineHour = round( $lineHour / count($werte_single_lines) );
		  if($x>0 && isset($last_h) && $last_h!=date('H',$timestamp) && date('H',$timestamp)%$lineHour==0 && $countBalken<601){
			  imagestring($theImage,2,$x,$imgHeight-25,"|".date('H',$timestamp), $black);
		  }
		  $last_h = date('H',$timestamp);
      
      $last_day = date('d.M.Y',$timestamp);
      $tObje->modify('+1 Hour');
      $timestamp = $tObje->getTimestamp();
    }

	  #draw diagram
	  $k=0;
	  rsort($werte_single_lines);#otherwise color of graph changing
	
	  foreach($werte_single_lines as $t_id=>$lineWerte){
		  //ksort($lineWerte);
		  if($t_id!="")$k++;
		  $i = 0;
		  $last_day=0;
		  $last_h=0;
		  $last_timestamp = null;
		  $count_measure_gaps=0;
            ksort($lineWerte);
		  foreach($lineWerte as $timestamp=>$w){
			if(next($lineWerte)===FALSE)continue; //end
			$value_next = current($lineWerte);
			$timestamp_next = key($lineWerte);
			  $y = @round($dynHeight * $w);
			  $y_next = @round($dynHeight * $value_next);
			  $x = @round($eachSeconds_inPixel * ($timestamp-$timestampStart) );
			  $x_next = @round($eachSeconds_inPixel * ($timestamp_next-$timestampStart));

			  //draw line
			  if($i+1 < $countBalken){
				  $col = $colorBlue; 
				  if($t_id!="" && $k==1)$col=$colorDiagramm_1;
				  if($t_id!="" && $k==2)$col=$colorDiagramm_2;
				  if($t_id!="" && $k==3)$col=$colorDiagramm_3;
				  if($t_id!="" && $k==4)$col=$colorDiagramm_4;
				  if($t_id!="" && $k==5)$col=$colorDiagramm_5;
				  $diff = round(($timestamp_next-$timestamp-(60*10))/100)*100/60;
				  if( $diff>$default_measureGap_inMin){ 
				  	$col=$colorOrange1; $count_measure_gaps++;
				  }
				  //echo "<br>$k a".($height-$y).';'.($height-$y_next).";";
				  imageline($theImage, $x, ($height-$y), $x_next, ($height-$y_next), $col);
			  }

			  $i++;
			  $last_timestamp = $timestamp;	
		  }
	  }
	  //imagestring($theImage,2,$x-270,$height+56,"due to possible measure gaps possible time-distortion/strain", $colorGreyBlack);
	  imagestring($theImage,2,5,$height+56,"last ".$countBalken." entrys", $colorGreyBlack);
	  if(isset($_GET['t_id']) && $_GET['t_id']!='')imagestring($theImage,2,$width-5-(strlen($_GET['t_id'])*5),$height+56,"id ".$_GET['t_id']."", $colorGreyBlack);
	  if($count_measure_gaps>0) imagestring($theImage,2,$x-670,$height+56,"contain offline-times => graph scratch in width (not compare-able)", $colorGreyBlack);
	  header("Content-type: image/png");
	  imagepng($theImage);
	  imagedestroy($theImage);
}

?>
