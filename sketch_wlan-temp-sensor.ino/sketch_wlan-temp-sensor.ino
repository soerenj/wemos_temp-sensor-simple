#include <OneWire.h>
#include <ESP8266WiFi.h>

//upload temperture sensor values to webserver
//project: https://gitlab.com/soerenj/wemos_temp-sensor-simple
//based on work, of different authors, so different licence?
//myparts: under AGPL / GPL2/3, or whatever...


//works for multiple Temperature sensor
//written in Ardunio IDE for Wemos with Temperature Sensor. Upload data own Website (see the php-script file)
//You must own an Website-Server with PHP
//alternative:, you could chouse a serivce like emoncms (example script look: https://gist.github.com/soerenj/a25e7c0d455c443cfe710d7e8fbb35c8)

const char* ssid = ""; //Wlan SSID
const char* password = ""; //Wlan Password

const char* host = "mydomain.com"; //without http://  (filename on server:  	temp-measure.php)
//not in use const char* privateKey = "xxx";
const int httpPort = 80;

//Seconds to wait between measure 300 = 5Min
const int sleepTimeInSec = 3550; //3600=every 60 Minutes;

// ? OneWire DS18S20, DS18B20, DS1822 Temperature Example
// ? http://www.pjrc.com/teensy/td_libs_OneWire.html
// Download and copy this OneWire Libary in yout Libary folder (Ardunio -> libraries/OneWire/)
// https://github.com/PaulStoffregen/OneWire
// ? The DallasTemperature library can do all this work for you!
// ? http://milesburton.com/Dallas_Temperature_Control_Library

OneWire  ds(D2);  // on pin D2 (a 4.7K resistor is necessary)

void setup(void) {
  Serial.begin(115200);
  delay(10); //besser mehr (orginal 10)?

  Serial.println(" SourceCode: https://gitlab.com/soerenj/wemos_temp-sensor-simple ");
  //alt:Serial.println(" Sensor-Daten: https://emoncms.org/dashboard/view?id=XXXXXXXX ");
  Serial.println();
  connectWlan();
  
}

void connectWlan(){
  WiFi.mode(WIFI_STA);
  
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  // Print the IP address
  Serial.print("Local IP : ");
  Serial.println(WiFi.localIP());
}


// ==============================================
// Send Data to emoncms
// ==============================================
// Source: https://raspberry.tips/iot/low-cost-wlan-temperatursensoren-mit-nodemcu-esp8266-im-selbstbau
// New: based on: https://draeger-it.blog/wemos-d1-wlan-thermometer-teil2-upload-der-daten-in-eine-datenbank/
void sendData(String t_id , float celsius ) {
  WiFiClient hTTPClient;

  //Connect by http
  const int httpPort = 80;
  if (!hTTPClient.connect(host, httpPort)) {
    Serial.println("failed to connect to Host ");
    Serial.print(host);
    Serial.println("");
    return;
  }


  //Build emoncms URL for sending data to
  String url = "/temp-measure.php?temp=";
  url += celsius;
  //currently this ID of the Temperatur-Sensor is disable (debause my PHP-Script cant handle this information)
  url += "&t_id=";
  url += t_id;

  Serial.println("URL:");
  Serial.println(url);
  // Send the HTTP Request to the emoncms server
  hTTPClient.print(String("GET ") + url + " HTTP/1.1\r\n" +
                  "Host: " + host + "\r\n" +
                  "Connection: close\r\n\r\n");
  delay(500);

  hTTPClient.stop();
}



void loop(void) {
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  String t_id;
  
  if ( !ds.search(addr)) {
    Serial.println("No more addresses.");
    Serial.println();
    ds.reset_search();
    
    WiFi.disconnect();
    
    Serial.print(" pause (in Sek) ");
    Serial.println(sleepTimeInSec);
    delay(sleepTimeInSec*1000);
    
    
    Serial.begin(115200);
    delay(10); //besser mehr (orginal 10)?
  
    connectWlan();
     
    delay(250);
    return;
  }

  t_id = "";
  Serial.print("ROM =");
  for( i = 0; i < 8; i++) {
    Serial.write(' ');
    Serial.print(addr[i], HEX);
    t_id += String(addr[i], HEX);
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return;
  }
  Serial.println();
 
  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      Serial.println("Device is not a DS18x20 family device.");
      return;
  } 

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("  Temperature = ");
  Serial.print(celsius);
  
  Serial.println(" upload data: start");
  sendData(t_id, celsius);
  Serial.println(" upload data: ready");

  Serial.print(" Celsius, ");
  Serial.print(fahrenheit);
  Serial.println(" Fahrenheit");
}

